# Catch-all site

How to create a site in ISPConfig (or any Apache appliance) as "default server" site.

## The problem

Sometimes there is a problem with a specific site's configuration in Apache, e.g. https problems like Let's Encrypt misconfiguration, etc. In situations like this Apache decides to serve a "random" site (usually the most recently added) in place of the site asked originally. We can see which of our sites is the "default" site for the server using the command: `apache2ctl -S | grep "default server"`. This site is presented whenever someone asks Apache about a site that isn’t defined properly in its configuration. A common example is a site that has no SSL configuration and yet is requested as https.
This of course is very bad, because the visitor is presented with a totally different site under the name of the site requested!

## Use case

The (quick’n’dirty) solution below is useful when we want a simple “landing” page to catch all domain requests to our Apache server that cannot be resolved. For security and stability reasons, we may want to configure the server to completely deny such requests.

## A solution

In order to overcome the above situation, we could adjust the file `000-default.conf` in such a way that the server always uses a specific site as a fallback when something in another site's configuration goes wrong. That site doesn't have to be a real one, e.g. no DNS records or domain possession needed. It’s just a dummy site that is presented in case Apache doesn’t know what to serve.

## Steps

First, we have to create this site, lets name it “catch.all”. For convenience, we create it inside ISPConfig and we issue a (self-signed) SSL certificate for it.
But this site shouldn’t appear in ISPConfig sites list, we will configure it indepentently. So we copy the `ssl` directory of the site (`/var/www/catch.all/ssl`) to a tmp place and we delete the site from ISPConfig. Then we create  the same directory called `catch.all` in `var/www/` and we put the original `ssl` directory and a `web` directory with an `index.html` file inside. We may include our branding here, but we must have in mind that a lightweight appliance is much better than a heavy one.
After that, we create the directory for logs:
```
# mkdir /var/log/ispconfig/httpd/catch.all
```
Next we have to replace `/etc/apache2/sites-available/000-default.conf` with the one given here, keeping a copy of the original for easy reverting. The only things we must adjust are `<VirtualHost XXX.XXX.XXX.XXX:80>` and `<VirtualHost XXX.XXX.XXX.XXX:443>`, placing our own IP. We may further adjust this file to our needs. After an Apache reload, the command `apache2ctl -S | grep "default server"` should have an output like this:
```
# apache2ctl -S | grep "default server"
         default server catch.all (/etc/apache2/sites-enabled/000-default.conf:1)
         default server catch.all (/etc/apache2/sites-enabled/000-default.conf:19)
```
and from now on Apache should respond with the newly created site to any requests for sites that, for various reasons, cannot be served.

## Testing

To test the behavior, we may add “a-wrong-site.com” together with our server IP to our system’s (e.g. our laptop’s, not the server’s) host file. Then we point our browser to “http://a-wrong-site.com/”. We should see our “site not found” page under the address.
![](a-wrong-site.com.png)
